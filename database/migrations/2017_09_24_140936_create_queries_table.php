<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queries', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('user_telegram_id')->unsigned();
            $table->foreign('user_telegram_id')->references('telegram_id')->on('users');

            $table->timestamps();

            $table->json('filters')->nullable();

            $table->string('title')->nullable();
            $table->string('search_url')->nullable();
            $table->integer('frequency_type_id')->nullable()->default();

            $table->timestamp('last_checked_at', 0)->nullable();
            $table->timestamp('next_check_at', 0)->nullable();
            $table->bigInteger('last_ad_id')->unsigned()->nullable();

            $table->index('user_telegram_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queries');
    }
}
