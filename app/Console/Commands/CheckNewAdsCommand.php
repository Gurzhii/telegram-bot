<?php

namespace App\Console\Commands;

use App\Jobs\CheckNewAdsJob;
use App\Jobs\ParseLastAdIdJob;
use App\Query;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckNewAdsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'queries:check-new-ads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse new ads and sending them to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->toDateTimeString();
        $queries = Query::where('next_check_at', '<=', $now)->get();

        foreach ($queries as $query) {
            CheckNewAdsJob::dispatch($query)->onQueue('default');
        }
    }
}
