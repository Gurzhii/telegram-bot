<?php

namespace App\Console\Commands;

use App\Jobs\ParseLastAdIdJob;
use App\Query;
use Illuminate\Console\Command;

class ParseLastAdIdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'queries:parse-last-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse last ad id for the first time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $queries = Query::whereNull('last_ad_id')->get();

        foreach ($queries as $query) {
            ParseLastAdIdJob::dispatch($query)->onQueue('default');
        }
    }
}
