<?php

namespace App\Domain;

use BotMan\BotMan\Messages\Outgoing\Actions\Button;

class FrequencyButtons
{
    const DEFAULT_FREQUENCY = 8; //Once pre day

    public static $frequencies = [
        9 => 'раз в минуту',
        1 => 'раз в 5 минут',
        2 => 'раз в 15 минут',
        3 => 'раз в 30 минут',
        4 => 'раз в час',
        5 => 'раз в 2 часа',
        6 => 'раз в 4 часа',
        7 => 'раз в 12 часов',
        8 => 'раз в день',
    ];

    public static function all()
    {
        $buttons[] = Button::create('Раз в минуту')->value('9');
        $buttons[] = Button::create('Раз в 5 минут')->value('1');
        $buttons[] = Button::create('Раз в 15 минут')->value('2');
        $buttons[] = Button::create('Раз в 30 минут')->value('3');
        $buttons[] = Button::create('Раз в час')->value('4');
        $buttons[] = Button::create('Раз в 2 часа')->value('5');
        $buttons[] = Button::create('Раз в 4 часа')->value('6');
        $buttons[] = Button::create('Раз в 12 часов')->value('7');
        $buttons[] = Button::create('Раз в день')->value('8');

        return $buttons;
    }

    public static function secondsOf($frequency = null)
    {
        $frequency = ($frequency == null) ? static::DEFAULT_FREQUENCY : $frequency;

        $frequencies = [
            9 => 1 * 60,
            1 => 5 * 60,
            2 => 15 * 60,
            3 => 30 * 60,
            4 => 60 * 60,
            5 => 2 * 60 * 60,
            6 => 4 * 60 * 60,
            7 => 12 * 60 * 60,
            8 => 24 * 60 * 60,
        ];

        return $frequencies[$frequency];
    }
}
