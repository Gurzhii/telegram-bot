<?php

namespace App\Domain\Conversations;

use App\Domain\FrequencyButtons;
use App\Query;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class SingleQueryConversation extends Conversation
{
    protected $query;
    protected $model;

    public function __construct($query)
    {
        $this->query = Query::where('id', $query)->first();
    }

    public function run()
    {
        $this->start();
    }

    protected function start()
    {
        $this->bot->types();
        $frequency = FrequencyButtons::$frequencies[$this->query->frequency_type_id];
        $buttons = [
            Button::create('Обновить ссылку на поиск')->value('update'),
            Button::create("Изменить частоту уведомлений (установлено - {$frequency})")->value('change_frequency'),
            Button::create('Не отслеживать')->value('delete')
        ];

        $question = Question::create("{$this->query->title} ({$this->query->search_url}). Что желаете сделать?")->addButtons($buttons);

        $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($this->isUpdate($answer->getValue())) {
                    $this->askForNewSearchUrl();
                } elseif ($this->isDelete($answer->getValue())) {
                    $this->query->delete();
                    $this->bot->reply("Вы больше не отслеживаете {$this->query->title}");
                } elseif ($this->isUpdateFrequency($answer->getValue())) {
                    $this->askForNewFrequency();
                }
            } else {
                $this->bot->reply('Выберите из списка!');
            }
        });
    }

    protected function askForNewSearchUrl()
    {
        $this->ask("Введите новую ссылку на поиск с olx.ua\n(скопируйте с адресной строки браузера со всеми фильтрами, которые вас интересуют)", function (Answer $answer) {
            if (! urlContains($answer->getText())) {
                $this->bot->reply('Неверно указана ссылка!');
            } else {
                $this->query->search_url = $answer->getText();
                $this->query->title = $answer->getText();
                $this->query->save();

                $this->bot->reply("Ссылка обновлена!\nТеперь вы следите за ".$answer->getText());
            }
        });
    }

    protected function askForNewFrequency()
    {
        $question = Question::create("С какой частотой присылать вам уведомления для {$this->query->title}?")
            ->addButtons(FrequencyButtons::all());

        $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $frequency_answer = $answer->getValue();
            } else {
                $frequency_answer = FrequencyButtons::DEFAULT_FREQUENCY;
            }
            $this->query->update(['frequency_type_id' => $frequency_answer]);
            $frequency_text = FrequencyButtons::$frequencies[$frequency_answer];

            $this->bot->reply("Теперь вы будете получать уведомления по {$this->query->title} {$frequency_text}");
        });
    }

    protected function isUpdateFrequency($answer)
    {
        return $answer == 'change_frequency';
    }

    protected function isUpdate($answer)
    {
        return $answer == 'update';
    }

    protected function isDelete($answer)
    {
        return $answer == 'delete';
    }
}
