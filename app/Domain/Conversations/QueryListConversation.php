<?php

namespace App\Domain\Conversations;

use App\Domain\FrequencyButtons;
use App\User;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class QueryListConversation extends Conversation
{
    protected $user;

    public function __construct($user)
    {
        $this->user = User::where('telegram_id', $user->getId())->first();
    }

    public function run()
    {
        $this->start();
    }

    protected function start()
    {
        $this->bot->types();
        $this->bot->reply($this->buildReply());
    }

    protected function buildReply()
    {
        $queries = $this->user->queries();

        if (! $queries->count()) {
            $this->noItemsReply();
        } else {
            $buttons = [];
            foreach ($queries->get() as $query) {
                $frequency = FrequencyButtons::$frequencies[$query->frequency_type_id];
                $buttons[] = Button::create("{$query->title} - {$frequency}")->value($query->id);
            }
            $question = Question::create("Выберите запрос:")->addButtons($buttons);

            $this->ask($question, function (Answer $answer) {
                if ($answer->isInteractiveMessageReply()) {
                    $this->bot->startConversation(new SingleQueryConversation($answer->getValue()));
                } else {
                    $this->bot->reply('Выберите из списка!');
                }
            });
        }
    }

    protected function noItemsReply()
    {
        $buttons = [
            Button::create('Отследить новый запрос')->value('new_query'),
            Button::create('Нет')->value('no')
        ];

        $question = Question::create("У вас нет отслеживаемых запросов, желаете добавить?")->addButtons($buttons);
        $this->bot->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() == 'new_query') {
                    $this->bot->startConversation(new NewQueryConversation($this->bot->getUser()));
                }
            }
        });
    }
}
