<?php

namespace App\Domain\Conversations;

use App\Domain\FrequencyButtons;
use App\User;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;

class NewQueryConversation extends Conversation
{
    protected $user;
    protected $query;

    public function __construct($user)
    {
        $this->user = User::where('telegram_id', $user->getId())->first();
    }

    public function run()
    {
        $this->start();
    }

    protected function start()
    {
        $this->bot->types();

        $this->ask("Введите ссылку на поиск с olx.ua\n(скопируйте из адресной строки браузера со всеми фильтрами, которые вас интересуют)", function (Answer $answer) {
            if (! urlContains($answer->getText())) {
                $this->bot->reply('Неверно указана ссылка!');
            } else {
                $this->query = $this->user->queries()->create([
                    'search_url' => $answer->getText(),
                    'title' => $answer->getText()
                ]);
                $this->askForFrequency();
            }
        });
    }

    protected function askForFrequency()
    {
        $question = Question::create("С какой частотой присылать вам уведомления для {$this->query->title}?")
            ->addButtons(FrequencyButtons::all());

        $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $frequency_answer = $answer->getValue();
            } else {
                $frequency_answer = FrequencyButtons::DEFAULT_FREQUENCY;
            }
            $this->query->update(['frequency_type_id' => $frequency_answer]);
            $frequency_text = FrequencyButtons::$frequencies[$frequency_answer];

            $this->bot->reply("Ссылка успешно добавлена!\nТеперь вы следите за {$this->query->title} и будете получать уведомления {$frequency_text}");
        });
    }
}
