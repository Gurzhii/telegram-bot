<?php

namespace App\Domain\Scraper;

use App\Domain\FrequencyButtons;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Cache\LaravelCache;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use BotMan\Drivers\Telegram\TelegramDriver;
use Carbon\Carbon;
use Goutte\Client;

class Olx
{
    protected $query;
    protected $crawler;

    public function __construct($query)
    {
        $this->query = $query;

        $client = new Client();
        $this->crawler = $client->request('GET', $this->query->search_url);
    }

    public static function create($query)
    {
        return new static($query);
    }

    public function getLastAdId()
    {
        $list = $this->crawler->filter("table[summary='Объявление'],table[summary='Оголошення']")->each(function ($node) {
            return $node->attr('data-id');
        });

        $frequency = $this->query->frequency_type_id;
        $addSeconds = FrequencyButtons::secondsOf($frequency);

        $this->query->update([
            'last_ad_id' => collect($list)->max(),
            'last_checked_at' => Carbon::now()->toDateTimeString(),
            'next_check_at' => Carbon::now()->addSeconds($addSeconds)->toDateTimeString()
        ]);
    }

    public function checkNewAds()
    {
        if (! $this->checkIfSearchResultsAreEmpty()) {
            $list = collect($this->crawler->filter("table[summary='Объявление'],table[summary='Оголошення']")->each(function ($node) {
                $id = $node->attr('data-id');
                $href = $node->filter('.link.linkWithHash.detailsLink')->attr('href');
                if ($id > $this->query->last_ad_id) {
                    $href = explode('.html', $href);
                    return ['id' => $id, 'href' => "{$href[0]}.html"];
                }
                return false;
            }))->filter()->sortByDesc('id');

            $this->updateQuery();

            if ($list->count() > 0) {
                $this->sendMessage($list);
            }
        }
    }

    protected function updateQuery()
    {
        $frequency = $this->query->frequency_type_id;
        $addSeconds = FrequencyButtons::secondsOf($frequency);

        $this->query->update([
            'next_check_at' => Carbon::now()->addSeconds($addSeconds)->toDateTimeString(),
            'last_checked_at' => Carbon::now()->toDateTimeString()
        ]);
    }

    protected function sendMessage($list)
    {
        $this->query->update([
            'last_ad_id' => $list->first()['id'],
        ]);

        $config = [
            'telegram' => [
                'token' => env('TELEGRAM_TOKEN')
            ]
        ];
        DriverManager::loadDriver(TelegramDriver::class);
        $bot = BotManFactory::create($config, new LaravelCache());
        $frequency_text = FrequencyButtons::$frequencies[$this->query->frequency_type_id];

        $message = "По вашему запросу \"{$this->query->title}\" {$frequency_text} ({$this->query->search_url}) поступили обновления:\n";

        foreach ($list->all() as $update) {
            $message .= $update['href']."\n";
        }

        $bot->say(new OutgoingMessage($message), $this->query->user_telegram_id, TelegramDriver::class);
    }

    protected function checkIfSearchResultsAreEmpty()
    {
        // div class for no results block, another ad = .emptynew
        return $this->crawler->filter('.emptynew')->count();
    }
}
