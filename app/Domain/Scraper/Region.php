<?php

namespace App;

class Region
{
    protected $list = [
        'vin' => [
            'slug' => 'vin',
            'title' => 'Винницкая область'
        ],
        'vol' => [
            'slug' => 'vol',
            'title' => 'Волынская область'
        ],
        'dnp' => [
            'slug' => 'dnp',
            'title' => 'Днепропетровская область'
        ],
        'don' => [
            'slug' => 'don',
            'title' => 'Донецкая область'
        ],
        'zht' => [
            'slug' => 'zht',
            'title' => 'Житомирская область'
        ],
        'zak' => [
            'slug' => 'zak',
            'title' => 'Закарпатская область'
        ],
        'zap' => [
            'slug' => 'zap',
            'title' => 'Запорожская область'
        ],
        'if' => [
            'slug' => 'if',
            'title' => 'Ивано-Франковская область'
        ],
        'ko' => [
            'slug' => 'ko',
            'title' => 'Киевская область'
        ],
        'kir' => [
            'slug' => 'kir',
            'title' => 'Кировоградская область'
        ],
        'cri' => [
            'slug' => 'cri',
            'title' => 'Крым'
        ],
        'lug' => [
            'slug' => 'lug',
            'title' => 'Луганская область'
        ],
        'lv' => [
            'slug' => 'lv',
            'title' => 'Львовская область'
        ],
        'nik' => [
            'slug' => 'nik',
            'title' => 'Николаевская область'
        ],
        'od' => [
            'slug' => 'od',
            'title' => 'Одесская область'
        ],
        'pol' => [
            'slug' => 'pol',
            'title' => 'Полтавская область'
        ],
        'rov' => [
            'slug' => 'rov',
            'title' => 'Ровенская область'
        ],
        'sum' => [
            'slug' => 'sum',
            'title' => 'Сумская область'
        ],
        'ter' => [
            'slug' => 'ter',
            'title' => 'Тернопольская область'
        ],
        'kha' => [
            'slug' => 'kha',
            'title' => 'Харьковская область'
        ],
        'khe' => [
            'slug' => 'khe',
            'title' => 'Херсонская область'
        ],
        'khm' => [
            'slug' => 'khm',
            'title' => 'Хмельницкая область'
        ],
        'chk' => [
            'slug' => 'chk',
            'title' => 'Черкасская область'
        ],
        'chn' => [
            'slug' => 'chn',
            'title' => 'Черниговская область'
        ],
        'chv' => [
            'slug' => 'chv',
            'title' => 'Черновицкая область'
        ]
    ];
}
