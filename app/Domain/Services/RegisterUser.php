<?php

namespace App\Domain\Services;

use App\User;

class RegisterUser
{
    public static function register($user)
    {
        return User::firstOrCreate([
            'telegram_id' => $user->getId()
        ], [
            'username' => $user->getUsername(),
            'telegram_id' => $user->getId()
        ]);
    }
}
