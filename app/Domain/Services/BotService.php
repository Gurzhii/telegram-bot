<?php

namespace App\Domain\Services;

use App\Domain\Conversations\NewQueryConversation;
use App\Domain\Conversations\QueryListConversation;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Cache\LaravelCache;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;

class BotService
{
    protected $botman;
    protected $keyboard = [
        ['Мои запросы 🗒', 'Новый запрос 🛠'],
        ['О боте', 'Поддержка'],
    ];

    public function __construct()
    {
        $config = [
            'telegram' => [
                'token' => env('TELEGRAM_TOKEN')
            ]
        ];

        DriverManager::loadDriver(TelegramDriver::class);
        $this->botman = BotManFactory::create($config, new LaravelCache);
    }
    
    public function run()
    {
        $this->botman->hears('/start|О боте', function () {
            RegisterUser::register($this->botman->getUser());
            $this->defaultAnswer();
        });

        $this->botman->hears('Поддержка', function () {
            $this->botman->reply("Для поддержки - обращаться к @Gurzhii");
        });

        $this->botman->hears('Мои запросы 🗒', function (BotMan $bot) {
            $bot->startConversation(new QueryListConversation($this->botman->getUser()));
        });

        $this->botman->hears('Новый запрос 🛠', function (BotMan $bot) {
            $bot->startConversation(new NewQueryConversation($this->botman->getUser()));
        });

        $this->botman->fallback(function () {
            $this->defaultAnswer(true);
        });

        $this->botman->listen();
    }

    public function defaultAnswer($silent = false)
    {
        $this->botman->types();
        $text = ($silent) ? $this->silentText() : $this->greetingsText();

        $this->botman->reply($text, [
            'reply_markup' => json_encode([
                'keyboard' => $this->keyboard,
                'one_time_keyboard' => false,
                'resize_keyboard' => true
            ])
        ]);
    }

    protected function silentText()
    {
        return "Что желаете сделать?";
    }

    protected function greetingsText()
    {
        return "Привет:) Это Olx ding bot!\nТут ты можешь получать уведомления о поступлении новых предложений для интересующих тебя товаров на OlxUA.\n\nДавай начнем! Что желаешь сделать?";
    }
}
