<?php

namespace App\Http\Controllers;

use App\Domain\Services\BotService;

class BotmanController extends Controller
{
    public function index()
    {
        return (new BotService)->run();
    }
}
