<?php

if (! function_exists('urlContains')) {
    function urlContains($haystack, $needle = 'olx.ua')
    {
        return strpos($haystack, $needle) !== false;
    }
}
