<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    protected $guarded = [];

    public function setSearchUrlAttribute($value)
    {
        //https://www.olx.ua/i2/gaysin/q-mjlt2/?search%5Bregion_id%5D=24&search%5Bcity_id%5D=17&search%5Bdist%5D=0&search%5Border%5D=created_at%3Adesc&currency=UAH&view=gallery @todo: cover this case
        if (!preg_match("~^(?:f|ht)tps?://~i", $value)) {
            $url = "https://" . $value;
        } else {
            $url = $value;
        }
        $this->attributes['search_url'] = $url;
    }

    public function setTitleAttribute($value)
    {
        $value = urldecode($value);

        $l = explode('/?search', $value);
        $this->attributes['title'] = "{$l[0]}...";
    }
}
